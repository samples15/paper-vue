// Services/Api
import {
  peopleRequest,
} from '../services/api';

// Export
export default {
  namespaced: true,

  // State
  state: {
    people: localStorage.getItem('people') ? JSON.parse(localStorage.getItem('people')) : [],
  },

  // Getters
  getters: {
    peopleList: (state) => state.people,
  },

  // Actions
  actions: {
    requestPeople({ commit }) {
      return new Promise((resolve, reject) => {
        peopleRequest()
          .then((res) => {
            commit('setPeople', res.data.results);
            localStorage.setItem('people', JSON.stringify(res.data.results));
            resolve(res.data.results);
          })
          .catch((err) => {
            reject(err);
          });
      });
    },
  },

  // Mutations
  mutations: {
    setPeople(state, payload) {
      state.people = payload;
    },
  },
};
