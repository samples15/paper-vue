// Services/Api
import {
  starshipsRequest,
} from '../services/api';

// Export
export default {
  namespaced: true,

  // State
  state: {
    starships: localStorage.getItem('starships') ? JSON.parse(localStorage.getItem('starships')) : [],
  },

  // Getters
  getters: {
    starshipsList: (state) => state.starships,
  },

  // Actions
  actions: {
    requestStarships({ commit }) {
      return new Promise((resolve, reject) => {
        starshipsRequest()
          .then((res) => {
            commit('setStarships', res.data.results);
            localStorage.setItem('starships', JSON.stringify(res.data.results));
            resolve(res.data.results);
          })
          .catch((err) => {
            reject(err);
          });
      });
    },
  },

  // Mutations
  mutations: {
    setStarships(state, payload) {
      state.starships = payload;
    },
  },
};
