// Vue
import Vue from 'vue';
import Vuex from 'vuex';

// Modules
import peopleStore from './people';
import starshipsStore from './starships';

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    people: peopleStore,
    starships: starshipsStore,
  },
});

export default store;
