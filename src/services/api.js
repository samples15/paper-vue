import axios from 'axios';

//-------------------------------
// Data Requests
//-------------------------------

export const peopleRequest = () => new Promise((resolve, reject) => {
  const api = 'https://swapi.dev/api/people';
  axios.get(api)
    .then((res) => {
      resolve(res);
    })
    .catch((err) => {
      reject(err);
    });
});


export const starshipsRequest = () => new Promise((resolve, reject) => {
  const api = 'https://swapi.dev/api/starships';
  axios.get(api)
    .then((res) => {
      resolve(res);
    })
    .catch((err) => {
      reject(err);
    });
});
